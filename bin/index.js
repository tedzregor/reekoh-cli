#! /usr/bin/env node
'use strict'

const packageJson = require('../package.json')

global.Promise = require('bluebird')
let program = require('commander')
let service = require('../service')
let command = require('../command')
let processArgs = process.argv

program
  .version(packageJson.version)
  .description(packageJson.description)
  .on('*', function (command) {
    command = command.shift()

    this.commands.some(cmd => {
      return cmd._name === command
    }) || service.handleUnknownCommand(command)
  })

program
  .command('login')
  .description('Login to Reekoh Market Place')
  .option('-a, --account <String>', 'Account')
  .option('-u, --username <String>', 'Username')
  .option('-p, --password <String>', 'Password')
  .action(command.login)

program
  .command('logout')
  .action(command.logout)

program
  .command('publish [PATH]')
  .description('Publish Reekoh plugin from a YML file.')
  .action(command.publish)

program
  .command('switchRole')
  .alias('sr')
  .description('Switch to another account role.')
  .action(command.switchRole)

program
  .command('update [PATH]')
  .description('Update Reekoh plugin from a YML file.')
  .action(command.updatePlugin)

program.parse(processArgs)

if (!processArgs.slice(2).length) {
  program.help()
}
