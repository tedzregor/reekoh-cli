'use strict'

const config = require('./config.json')

let _ = require('lodash')
let Service = require('./service')
let service = new Service()
let path = require('path')

class Command {
  login (options) {
    let data = {}

    service.getEnvironment().then(env => {
      service.env = env
      return service.getUserCredentials(options)
    }).then(credentials => {
      if (_.isEmpty(credentials)) {
        process.exit()
      }
      data.credentials = credentials
      credentials.email = credentials.username
      return service.userAuth(credentials)
    }).then(({idToken}) => {
      return service.getUserRoles(idToken)
    }).then(userRoles => {
      data.userRoles = userRoles
      return service.selectUserRole(userRoles)
    }).then(userRole => {
      data.selectedUserRole = userRole

      let {credentials} = data
      credentials.account = userRole.account._id
      credentials.role = userRole.role._id
      return service.userAuth(credentials)
    }).then(token => {
      let {username, account, role} = data.credentials
      Object.assign(token, {
        username,
        account,
        role,
        env: service.env
      })

      return service.saveUserData(token)
    }).then(() => {
      console.log('Login succeeded')
    }).catch(service.handleError)
    .catch(service.processExit)
  }

  switchRole () {
    let data = {}

    service.getUserData().then(userData => {
      service.env = userData.env
      Object.assign(data, userData)
      return service.getUserRoles(userData.idToken)
    }).then(accountRoles => {
      return service.selectUserRole(accountRoles)
    }).then(({account, role}) => {
      return service.switchRole({
        account: account._id,
        role: role._id
      }, data.idToken)
    }).then(newToken => {
      Object.assign(data, newToken)
      return service.saveUserData(data)
    }).then(() => {
      console.log('Successfully switched to selected account role.')
    }).catch(service.handleError)
    .catch(service.processExit)
  }

  logout () {
    service.removeUserData().then(() => {
      console.log('Logged out successfully')
    }).catch(service.handleError)
    .catch(service.processExit)
  }

  updatePlugin (pathToManifest) {
    pathToManifest = path.resolve(process.cwd(), pathToManifest || config.reekohYmlFile)

    let manifestDir = path.dirname(pathToManifest)
    let data = {}

    service.getUserData().then((userData) => {
      service.env = userData.env
      data.token = userData.idToken
      return service.getPluginDetails(pathToManifest)
    }).then((plugin) => {
      let icon = {
        manifestDir,
        key: 'metadata.icon',
        path: './icon.png',
        validExtensions: ['jpeg', 'png', 'jpg', 'gif']
      }
      return service.validateFile(icon, plugin)
    }).then((plugin) => {
      return service.saveIcon({
        manifestDir,
        token: data.token
      }, plugin)
    }).then((plugin) => {
      return service.submitPlugin(data.token, 'PATCH', plugin)
    }).then(() => {
      console.log('Plugin updated')
    }).catch(service.handleError)
    .catch(service.processExit)
  }

  publish (pathToManifest) {
    pathToManifest = path.resolve(process.cwd(), pathToManifest || config.reekohYmlFile)
    let manifestDir = path.dirname(pathToManifest)
    let data = {}

    service.getUserData().then((userData) => {
      service.env = userData.env
      data.token = userData.idToken
      return service.getPluginDetails(pathToManifest)
    }).then(plugin => {
      let notes = {
        manifestDir,
        key: 'metadata.release.notes',
        path: './RELEASENOTES.md',
        validExtensions: ['md'],
        getContent: true
      }

      return service.validateFile(notes, plugin)
    }).then((plugin) => {
      let documentation = {
        manifestDir,
        key: 'metadata.release.documentation',
        path: './README.md',
        validExtensions: ['md'],
        getContent: true
      }

      return service.validateFile(documentation, plugin)
    }).then((plugin) => {
      let icon = {
        manifestDir,
        key: 'metadata.icon',
        path: './icon.png',
        validExtensions: ['jpeg', 'png', 'jpg', 'gif']
      }
      return service.validateFile(icon, plugin)
    }).then((plugin) => {
      return service.saveIcon({
        manifestDir,
        token: data.token
      }, plugin)
    }).then((plugin) => {
      return service.savePluginCode({
        manifestDir,
        token: data.token
      }, plugin)
    }).then((plugin) => {
      return service.submitPlugin(data.token, 'POST', plugin)
    }).then(() => {
      console.log(`Plugin submitted`)
    }).catch(service.handleError)
    .catch(service.processExit)
  }
}

module.exports = new Command()
